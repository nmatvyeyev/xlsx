(defpackage #:xlsx
  (:use #:cl)
  (:export #:list-sheets #:read-sheet #:as-matrix))

;;;; (declaim (optimize (compilation-speed 0) (debug 3) (safety 0) (space 0) (speed 0)))
